package me.travel.model

data class Snippet(val user: String, val text: String)