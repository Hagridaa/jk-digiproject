package me.travel.model

import java.time.LocalDate

data class Booking(val user: User, val destination: String, val date: LocalDate)