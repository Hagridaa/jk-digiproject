package me.travel.model

data class Mail(val user: User, val message: String)