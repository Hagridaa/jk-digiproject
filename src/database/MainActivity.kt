package me.travel.database

import com.google.firebase.database.*
import com.google.gson.Gson
import me.travel.model.Booking
import me.travel.model.Mail
import me.travel.model.User
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.LocalDate
import java.util.*
import kotlin.random.Random

class MainActivity  {

    companion object {
        val log: Logger = LoggerFactory.getLogger(MainActivity::class.java)

        private const val TAG = "KotlinActivity"
    }

    fun userWrite(user: User) {
        try {

            val database: FirebaseDatabase = FirebaseDatabase.getInstance()
            val ref: DatabaseReference = database.getReference("app")
            val usersRef = ref.child("users")
            val gson = Gson()
            val jsonUser = gson.toJson(user)
            log.debug("Sending new user to database: $jsonUser")
            usersRef.setValueAsync(user.toString())
/*            usersRef.setValueAsync(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    val userValue = dataSnapshot.getValue(User::class.java)
                    log.info(TAG, "Value is: $userValue")
                }

                override fun onCancelled(error: DatabaseError) {
                    // Failed to read value
                    log.error(TAG, "Failed to read value.", error.toException())
                }
            })*/
        } catch (e: RuntimeException) {
            log.error("Writing user failed!", e)
            throw RuntimeException("Writing user failed!", e)
        }
    }


    fun bookingWrite(booking: Booking) {
        try {
            val database: FirebaseDatabase = FirebaseDatabase.getInstance()
            val ref: DatabaseReference = database.getReference("app")
            val bookingRef = ref.child("booking")
            val gson = Gson()
            val bookingJson = gson.toJson(booking)
            log.debug("Sending new booking to database: $bookingJson")
            bookingRef.setValueAsync(booking.toString())
  /*          bookingRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    val bookingValue = dataSnapshot.getValue(Booking::class.java)
                    log.info(TAG, "Value is: $bookingValue")
                }

                override fun onCancelled(error: DatabaseError) {
                    // Failed to read value
                    log.error(TAG, "Failed to read value.", error.toException())
                }
            })*/
        } catch (e: RuntimeException) {
            log.error("Writing user failed!", e)

        }
    }

    fun mailWrite(mail: Mail) {
        try {
            val database: FirebaseDatabase = FirebaseDatabase.getInstance()
            val ref: DatabaseReference = database.getReference("app")
            val mailRef = ref.child("mail")
            val gson = Gson()
            val bookingJson = gson.toJson(mail)
            log.debug("Sending new booking to database: $bookingJson")
            mailRef.setValueAsync(bookingJson)
            mailRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    val post = dataSnapshot.getValue(Mail::class.java)
                    log.info(TAG, "Value is: $post")
                }

                override fun onCancelled(error: DatabaseError) {
                    // Failed to read value
                    log.error(TAG, "Failed to read value.", error.toException())
                }
            })
        } catch (e: RuntimeException) {
            log.error("Writing user failed!", e)
        }
    }

    fun basicReadWrite() {
        // [START write_message]
        // Write a message to the database
        val database = FirebaseDatabase.getInstance()
        val ref = database.getReference("app")
        val date = LocalDate.of(2019, 9, 15)
        val testUser = User("Testi", "test@test.com", "test")
        val secondUser = User("Testi2", "test1@testi1.com", "test1")
        val thirdUser = User("Testi3", "test3@test3.com", "testi3")
        val fourthFellow = User("Fellow Fourth", "Fellow@test3.com", "testi3")
        val users = TreeMap<Int, User>()
        users[1] = testUser
        users[2] = secondUser
        users[3] = thirdUser
        val gson = Gson()
        val usersRef = ref.child("users")
/*        users.forEach {
            val json = gson.toJson(it)
            usersRef.setValueAsync(json)
        }*/



        val bookingRef = ref.child("bookings")
/*        val firstBooking = Booking(testUser, "Japan", date)
        val secondBooking = Booking(secondUser, "South-Korea", LocalDate.of(2019, 10, 19))
        val thirdBooking = Booking(thirdUser, "Japan", LocalDate.of(2019, 12, 19))

        val availableBookings: List<Booking> = mutableListOf(
            firstBooking,
            secondBooking,
            thirdBooking
        )
        availableBookings.forEach {
            val json = gson.toJson(it)
            bookingRef.setValueAsync(json)
        }*/

        val readAndWriteBooking = ReadAndWriteBooking(bookingRef)
        readAndWriteBooking.writeNewBooking(Random.nextInt().toString(), fourthFellow, "South-Korea", LocalDate.of(2019, 7, 5))
        // [END write_message]

        // [START read_message]
        // Read from the database
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                val value = dataSnapshot.getValue(String::class.java)
                log.info(TAG, "Value is: $value")
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                log.error(TAG, "Failed to read value.", error.toException())
            }
        })
        // [END read_message]
    }
}