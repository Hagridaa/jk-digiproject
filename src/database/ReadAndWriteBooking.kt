package me.travel.database

import com.google.firebase.database.DatabaseReference
import me.travel.model.Booking
import me.travel.model.User
import java.time.LocalDate

class ReadAndWriteBooking(private val database: DatabaseReference) {
    fun writeNewBooking(bookingId: String, user: User, destination: String, date: LocalDate) {
        val booking = Booking(user, destination, date)

        database.child("bookings").child(bookingId).setValueAsync(booking)
    }
}