package me.travel.database

import com.google.firebase.database.DatabaseReference
import me.travel.model.User

class ReadAndWriteUser(private val database: DatabaseReference) {

    fun writeNewUser(userId: String, name: String, email: String, passw: String) {
        val user = User(name, email, passw)

        database.child("users").child(userId).setValueAsync(user)
    }
}