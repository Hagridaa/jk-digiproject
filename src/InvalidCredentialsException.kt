package me.travel

import java.lang.RuntimeException

class InvalidCredentialsException(message: String) : RuntimeException(message)