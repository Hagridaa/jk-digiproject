package me.travel

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.database.DatabaseException
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.jwt
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.html.respondHtml
import io.ktor.http.*
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.gson.gson
import io.ktor.request.receive
import io.ktor.request.receiveOrNull
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.sessions.*
import io.ktor.util.pipeline.PipelineContext
import kotlinx.css.*
import kotlinx.html.*
import me.travel.database.MainActivity
import me.travel.database.MainActivity.Companion.log
import me.travel.model.Booking
import me.travel.model.Mail
import me.travel.model.Snippet
import me.travel.model.User
import me.travel.page.*
import java.io.FileInputStream
import java.lang.Exception
import java.text.DateFormat
import java.time.LocalDate
import java.util.*
import kotlin.collections.List
import kotlin.collections.MutableList
import kotlin.collections.mapOf
import kotlin.collections.mutableListOf
import kotlin.collections.plusAssign
import kotlin.collections.set
import kotlin.collections.toList

val snippets: MutableList<Snippet> = Collections.synchronizedList(
    mutableListOf(
        Snippet(user = "test", text = "hello"),
        Snippet(user = "test", text = "world")
    )
)

@Suppress("unused") // Referenced in application.conf
fun Application.main() {
    getServiceAccountAccessToken()
    val simpleJWT = SimpleJWT("my-super-secret-for-jwt")

    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        allowCredentials = true
        anyHost()
    }

    install(Authentication) {
        form("login") {
            userParamName = "username"
            passwordParamName = "password"
            challenge = FormAuthChallenge.Unauthorized
            validate { credentials ->
                if (credentials.name == credentials.password)
                    UserIdPrincipal(credentials.name) else null
            }
        }

        jwt {
            verifier(simpleJWT.verifier)
            validate {
                UserIdPrincipal(it.payload.getClaim("name").asString())
            }
        }
    }

    install(Sessions) {
        cookie<UserSession>("SESSION") {
            cookie.extensions["SameSite"] = "lax"
        }
    }

    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }

    install(StatusPages) {
        exception<InvalidCredentialsException> { exception ->
            call.respond(
                HttpStatusCode.Unauthorized,
                mapOf("OK" to false, "error" to (exception.message ?: ""))
            )
        }
        exception<AuthorizationException> { cause ->
            call.respond(HttpStatusCode.Forbidden)
        }

        exception<DatabaseException> { cause ->
            call.respond(HttpStatusCode.Forbidden)
        }
    }

    routing {
        get("/") {
            errorAware {
                call.respondHtml {
                    homePage()
                }
            }

        }

        get("/destinations") {
            errorAware {
                call.respondHtml {
                    destinationsPage()
                }
            }

        }

        route("/snippets") {
            get {
                call.respond(mapOf("snippets" to synchronized(snippets) {
                    snippets.toList()
                }))
            }

            authenticate {
                post {
                    val post = call.receive<PostSnippet>()
                    val principal = call.principal<UserIdPrincipal>() ?: error(
                        "No principal"
                    )
                    snippets += Snippet(principal.name, post.snippet.text)
                    call.respond(mapOf("OK" to true))
                }
            }
        }

        get("/book") {
            errorAware {
                call.respondHtml {
                    bookPage()
                }
            }

        }

        post("/book") {
            val post = call.receiveOrNull() ?: Parameters.Empty
            val mainActivity = MainActivity()
            log.debug(post.toString())
            val fullName = post["fullName"]
            log.debug(fullName)
            val email = post["email"]
            val destination = post["destination"]
            log.debug("destination: $destination")
            try {
                /*if (fullName != null && fullName.isNotBlank() && fullName == email && destination != null && destination.isNotBlank()) {*/
                log.debug("Creating a new user..")
                val user = User(fullName!!, email!!, "")
                log.debug("User $user created")
                val booking = Booking(user, destination!!, LocalDate.now())
                log.debug("New booking: $booking created")
                mainActivity.bookingWrite(booking)
            } catch (e: RuntimeException) {
                return@post call.respondRedirect("/invalid", permanent = false)
            }
            call.respondHtml {
                defaultHead()
                body(classes = "tex-center") {
                    div(classes = "cover container d-flex w-100 h-100 p-3 mx-auto flex-column") {
                        bookNavigation()
                        main(classes = "mx-auto inner cover") {
                            h1(classes = "cover-heading") {
                                +"Your trip was booked successfully!"
                            }
                            ul(classes = "list-group") {
                                li(classes = "list-group-item bg-dark") {
                                    +"Full Name: "
                                    h3 {
                                        +"$fullName"
                                    }
                                }
                                li(classes = "list-group-item bg-dark") {
                                    +"Email: "
                                    h3 { +"$email" }
                                }

                                li(classes = "list-group-item bg-dark") {
                                    +"Destination: "
                                    h3 { +"$destination" }
                                }
                            }
                        }
                        defaultFooter()
                    }
                }
            }

//            } else {
//                call.respondHtml {
//                    body {
//                        +"Invalid details: $fullName, $email, $destination"
//                        a(href = "/book") { +"Retry?" }
//                    }
//                }
//            }
//                        part.dispose()
        }

        get("/gallery") {
            errorAware {
                call.respondHtml {
                    galleryPage()
                }
            }
        }

        get("/contact") {
            errorAware {
                call.respondHtml {
                    contactPage()
                }
            }
        }
        post("/contact") {
            errorAware {
                val post = call.receiveOrNull() ?: Parameters.Empty
                lateinit var user: User
                lateinit var mail: Mail
                log.debug(post.toString())
                val message = post["message"]
                val name = post["name"]
                val email = post["email"]
                val mainActivity = MainActivity()
                if (name != null && name.isNotBlank() && email != null && email.isNotBlank()) {
                    user = User(name, email, "")
                    mainActivity.userWrite(user)
                }
                if (message != null && message.isNotBlank()) {
                    mail = Mail(user, message)
                    mainActivity.mailWrite(mail)
                }
            }

        }

        get("/styles.css") {
            call.respondCss {
                body {
                    backgroundColor = Color("#0E2E1F")
                }
                p {
                    fontSize = 2.em
                }
                rule("p.myclass") {
                    color = Color.blue
                }
            }
        }

        route("/login") {


        }

        /*   post("/login-register") {
               val post = call.receive<LoginRegister>()
               val user = users.getOrPut(post.user) { User(post.user, "heippa@heippa.com", post.password) }
               if (user.password != post.password) throw InvalidCredentialsException("Invalid credentials")
               call.respond(mapOf("token" to simpleJWT.sign(user.name)))
           }*/

        // Static feature. Try to access `/static/ktor_logo.svg`
        static("/static") {
            resources("static")
        }
        static("/pics") {
            resources("pics")
        }

        get("/session/increment") {
            val session = call.sessions.get<MySession>() ?: MySession()
            call.sessions.set(session.copy(count = session.count + 1))
            call.respondText("Counter is ${session.count}. Refresh to increment.")
        }


    }

/*    val activity = MainActivity()
    activity.basicReadWrite()*/
}

fun getServiceAccountAccessToken() {
    log.info("initalizing firebase account")
    val serviceAccount =
        FileInputStream("testproject-fe067-firebase-adminsdk-l8twg-b291d7a70b.json")
    val options = FirebaseOptions.Builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .setDatabaseUrl("https://testproject-fe067.firebaseio.com")
        .build()
    FirebaseApp.initializeApp(options)
}


data class IndexData(val items: List<Int>)

data class MySession(val count: Int = 0)

data class UserSession(val username: String)

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()

fun FlowOrMetaDataContent.styleCss(builder: CSSBuilder.() -> Unit) {
    style(type = ContentType.Text.CSS.toString()) {
        +CSSBuilder().apply(builder).toString()
    }
}

fun CommonAttributeGroupFacade.style(builder: CSSBuilder.() -> Unit) {
    this.style = CSSBuilder().apply(builder).toString().trim()
}

suspend inline fun ApplicationCall.respondCss(builder: CSSBuilder.() -> Unit) {
    this.respondText(CSSBuilder().apply(builder).toString(), ContentType.Text.CSS)
}

private suspend fun <R> PipelineContext<*, ApplicationCall>.errorAware(block: suspend () -> R): R? {
    return try {
        block()
    } catch (e: Exception) {
        call.respondText(
            """{"error":"$e"}""",
            ContentType.parse("application/json"),
            HttpStatusCode.InternalServerError
        )
        null
    }
}

private suspend fun ApplicationCall.respondSuccessJson(value: Boolean = true) = respond("""{"success": "$value"}""")