package me.travel

data class PostSnippet(val snippet: PostSnippet.Text) {
    data class Text(val text: String)
}