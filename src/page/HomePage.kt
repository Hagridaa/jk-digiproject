package me.travel.page

import kotlinx.html.*

fun HTML.homePage() {
    lang = "en"
    defaultHead()

    body(classes = "text-center") {
        div(classes = "cover container d-flex w-100 h-100 p-3 mx-auto flex-column") {
            header(classes = "masthead mb-auto") {
                div(classes = "inner") {
                    h3(classes = "masthead-brand") {
                        +"Travel App"
                    }
                    defaultNavigation()
                }
            }
            defaultMain()
            defaultFooter()

        }
        script(src = "https://code.jquery.com/jquery-3.2.1.slim.min.js") {}
        script(src = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js") {}
        script {
            src = "/static/bootstrap.js"
        }
        script {
            src = "/static/typeit.min.js"
        }
        script {
            src = "/static/typewriter.js"
        }
    }
}

private fun DIV.defaultMain() {
    main(classes = "inner cover") {
        h1(classes = "cover-heading") {
            p(classes = "lead") {
                +"Make your dreams come true and travel to Asia. We'll make your dreams happen."
            }
            p(classes = "lead") {
                a(href = "/book", classes = "btn btn-lg btn-secondary") {
                    +"Book trip now"
                }
            }
        }
    }
}

private fun DIV.defaultNavigation() {
    nav(classes = "nav nav-masthead justify-content-center") {
        a(href = "#", classes = "nav-link active") {
            +"Home"
        }
        a(href = "/book", classes = "nav-link") {
            +"Book"
        }
        a(href = "/destinations", classes = "nav-link") {
            +"Destinations"
        }
        a(href = "/gallery", classes = "nav-link") {
            +"Gallery"
        }
        a(href = "#", classes = "nav-link") {
            +"Contact"
        }
        a(href = "/login", classes = "nav-link") {
            +"Login"
        }
    }
}