package me.travel.page

import kotlinx.html.*

fun HTML.defaultHead() {
    head {
        title { +"Travel The Asia: Booking App" }
        meta { charset = "UTF-8" }
        meta(name = "viewport", content = "width=device-width, initial-scale=1, shrink-to-fit=no")
        styleLink("/static/bootstrap.min.css")
        styleLink("https://use.typekit.net/mgt4xuy.css")
        styleLink("/static/styles.css")
        styleLink("https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.6/css/mdb.min.css")
    }
}

fun DIV.defaultFooter() {
    footer(classes = "mastfoot mt-auto") {
        div(classes = "inner") {
            p {
                a(href = "#", classes = "btn-secondary") {
                    +"Travel App"
                }
                +" By Jussi And Kristiina"
            }
        }
    }
}

