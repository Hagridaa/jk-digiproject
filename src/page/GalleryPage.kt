package me.travel.page

import kotlinx.html.*

fun DIV.galleryNavigation() {
    nav(classes = "nav nav-masthead justify-content-center") {
        a(href = "/", classes = "nav-link") {
            +"Home"
        }
        a(href = "/book", classes = "nav-link") {
            +"Book"
        }
        a(href = "/destinations", classes = "nav-link") {
            +"Destinations"
        }
        a(href = "#", classes = "nav-link active") {
            +"Gallery"
        }
        a(href = "/contact", classes = "nav-link") {
            +"Contact"
        }
        a(href = "/login", classes = "nav-link") {
            +"Login"
        }
    }
}

fun HTML.galleryPage() {
    lang = "en"
    defaultHead()
    body(classes = "text-center") {
        div(classes = "container d-flex w-100 h-100 p-3 mx-auto flex-column") {
            header(classes = "masthead") {
                div(classes = "inner") {
                    h3 {
                        +"Travel app"
                    }
                    galleryNavigation()
                }
            }

            div(classes = "row") {
                div(classes = "col-sm-6") {
                    h2 { +"South-Korea, Seoul" }
                    h5 { +"Good food, beatiful country" }
                    img {
                        src = "/pics/seoul.jpg"
                        classes = setOf("img-thumbnail tooltip-test")
                        attributes["data-toggle"] = "modal"
                        attributes["data-target"] = "#exampleModal"
                        title = "Click here for more information"
                    }
                }
                div(classes = "col-sm-6") {
                    h2 { +"Japan, Tokyo" }
                    h5 { +"Unique city, amazing views" }
                    img {
                        src = "/pics/tokyo2.jpg"
                        classes = setOf("img-thumbnail tooltip-test")
                        attributes["data-toggle"] = "modal"
                        attributes["data-target"] = "exampleModal3"
                        title = "Click here for more information"
                    }
                }
            }

            /* Modal */
            div {
                classes = setOf("modal fade")
                id = "exampleModal"
                tabIndex = "-1"
                role = "dialog"
                attributes["data-target"] = "exampleModalLabel"
                attributes["aria-hidden"] = "true"

                div(classes = "modal-dialog") {
                    role = "document"
                    div(classes = "modal-dialog") {
                        div(classes = "modal-content") {
                            div(classes = "modal-header") {
                                h5(classes = "modal-title") {
                                    id = "exampleModalLabel"
                                    +"Seoul"
                                }
                                button {
                                    type = ButtonType.button
                                    classes = setOf("close")
                                    attributes["data-dismiss"] = "modal"
                                    attributes["aria-label"] = "Close"
                                    span {
                                        attributes["aria-hidden"] = "true"
                                    }
                                }
                            }
                            div(classes = "modal-body") {
                                +"7-day trip around Seoul"
                                a {
                                    href = "/book"
                                    button {
                                        type = ButtonType.button
                                        classes = setOf("btn btn-primary")
                                    }
                                }
                            }
                            div(classes = "modal-footer") {
                                button {
                                    type = ButtonType.button
                                    classes = setOf("btn btn-secondary")
                                    attributes["data-dismiss"] = "modal"
                                }
                            }
                        }
                    }
                }
            }

            div {
                classes = setOf("modal fade")
                id = "exampleModal3"
                tabIndex = "-1"
                role = "dialog"
                attributes["data-target"] = "exampleModalLabel"
                attributes["aria-hidden"] = "true"

                div(classes = "modal-dialog") {
                    role = "document"
                    div(classes = "modal-dialog") {
                        div(classes = "modal-content") {
                            div(classes = "modal-header") {
                                h5(classes = "modal-title") {
                                    id = "exampleModalLabel"
                                    +"Tokyo"
                                }
                                button {
                                    type = ButtonType.button
                                    classes = setOf("close")
                                    attributes["data-dismiss"] = "modal"
                                    attributes["aria-label"] = "Close"
                                    span {
                                        attributes["aria-hidden"] = "true"
                                    }
                                }
                            }
                            div(classes = "modal-body") {
                                +"Well take you around Tokyo."
                                a {
                                    href = "/book"
                                    button {
                                        type = ButtonType.button
                                        classes = setOf("btn btn-primary")
                                    }
                                }
                            }
                            div(classes = "modal-footer") {
                                button {
                                    type = ButtonType.button
                                    classes = setOf("btn btn-secondary")
                                    attributes["data-dismiss"] = "modal"
                                }
                            }
                        }
                    }
                }
            }

            defaultFooter()
        }

        script {
            src = "https://code.jquery.com/jquery-3.2.1.slim.min.js"
        }
        script {
            src =
                "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        }
        script {
            src = "/static/bootstrap.js"
        }
        script {
            src = "/static/carousel.js"
        }
    }
}