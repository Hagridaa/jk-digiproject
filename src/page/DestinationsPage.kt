package me.travel.page

import kotlinx.html.*

fun HTML.destinationsPage() {
    lang = "en"
    defaultHead()

    body(classes = "text-center") {
        div(classes = "container d-flex w-100 h-100 p-3 mx-auto flex-column") {
            header(classes = "masthead mb-auto") {
                div(classes = "inner") {
                    h3(classes = "masthead-brand") {
                        +"Travel App"
                    }
                    destinationsNavigation()
                }
            }

            div(classes = "row") {
                div(classes = "col-sm-6") {
                    h2 {
                        +"South Korea, Seoul"
                    }
                    h5 {
                        +"Good food, beatiful country"
                    }

                    img {
                        src = "pics/seoul.jpg"
                        classes = setOf("img-thumbnail tooltip-test")
                        attributes["data-toggle"] = "modal"
                        attributes["data-target"] = "#exampleModal"
                        title = "Click here for more information!"
                    }
                }
                div(classes = "col-sm-6") {
                    h2 {
                        +"Japan, Tokyo"
                    }
                    h5 {
                        +"unique city, amazing stuff"
                    }

                    img {
                        src = "pics/tokyo2.jpg"
                        classes = setOf("img-thumbnail tooltip-test")
                        attributes["data-toggle"] = "modal"
                        attributes["data-target"] = "#exampleModal"
                        title = "Click here for more information!"
                    }
                }
            }

            /* Modal */
            div {
                classes = setOf("modal fade")
                id = "exampleModal"
                tabIndex = "-1"
                attributes["aria-labelledby"] = "#exampleModalLabel"
                attributes["aria-hidden"] = "true"
                div(classes = "modal-dialog") {
                    role = "document"
                    div(classes = "modal-content") {
                        div(classes = "modal-header") {
                            h5(classes = "modal-title") {
                                id = "exampleModalLabel"
                                +"Seoul"
                            }
                            button {
                                type = ButtonType.button
                                classes = setOf("close")
                                attributes["data-dismiss"] = "modal"
                                attributes["aria-label"] = "Close"
                                span {
                                    attributes["aria-hidden"] = "true"
                                    +"X"
                                }
                            }
                        }
                        div(classes = "modal-body") {
                            +"7 day trip around Seoul"
                            a {
                                href = "/book"
                                button {
                                    type = ButtonType.button
                                    classes = setOf("btn btn-primary")
                                    +"Book now"
                                }
                            }
                        }
                        div(classes = "modal-footer") {
                            button {
                                type = ButtonType.button
                                classes = setOf("btn btn-secondary")
                                attributes["data-dismiss"] = "modal"
                                +"X"
                            }
                        }
                    }
                }
            }
            div {
                classes = setOf("modal fade")
                id = "exampleModal3"
                tabIndex = "-1"
                attributes["aria-labelledby"] = "#exampleModalLabel"
                attributes["aria-hidden"] = "true"
                div(classes = "modal-dialog") {
                    role = "document"
                    div(classes = "modal-content") {
                        div(classes = "modal-header") {
                            h5(classes = "modal-title") {
                                id = "exampleModalLabel"
                                +"Tokyo"
                            }
                            button {
                                type = ButtonType.button
                                classes = setOf("close")
                                attributes["data-dismiss"] = "modal"
                                attributes["aria-label"] = "Close"
                                span {
                                    attributes["aria-hidden"] = "true"
                                    +"X"
                                }
                            }
                        }
                        div(classes = "modal-body") {
                            +"Well take you around Tokyo"
                            a {
                                href = "/book"
                                button {
                                    type = ButtonType.button
                                    classes = setOf("btn btn-primary")
                                    +"Book now"
                                }
                            }
                        }
                        div(classes = "modal-footer") {
                            button {
                                type = ButtonType.button
                                classes = setOf("btn btn-secondary")
                                attributes["data-dismiss"] = "modal"
                                +"Close"
                            }
                        }
                    }
                }
            }
            defaultFooter()

        }
        script(src = "https://code.jquery.com/jquery-3.2.1.slim.min.js") {}
        script(src = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js") {}
        script {
            src = "/static/bootstrap.js"
        }
    }
}

private fun DIV.destinationsNavigation() {
    nav(classes = "nav nav-masthead justify-content-center") {
        a(href = "#", classes = "nav-link active") {
            +"Home"
        }
        a(href = "/book", classes = "nav-link") {
            +"Book"
        }
        a(href = "#", classes = "nav-link active") {
            +"Destinations"
        }
        a(href = "/gallery", classes = "nav-link") {
            +"Gallery"
        }
        a(href = "#", classes = "nav-link") {
            +"Contact"
        }
        a(href = "/login", classes = "nav-link") {
            +"Login"
        }
    }
}