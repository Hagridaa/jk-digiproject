package me.travel.page

import kotlinx.html.*

fun DIV.bookNavigation() {
    nav(classes = "nav nav-masthead justify-content-center") {
        a(href = "/", classes = "nav-link") {
            +"Home"
        }
        a(href = "/book", classes = "nav-link active") {
            +"Book"
        }
        a(href = "/destinations", classes = "nav-link") {
            +"Destinations"
        }
        a(href = "/gallery", classes = "nav-link") {
            +"Gallery"
        }
        a(href = "/contact", classes = "nav-link") {
            +"Contact"
        }
        a(href = "/login", classes = "nav-link") {
            +"Login"
        }
    }
}

fun HTML.bookPage() {
    lang = "en"
    defaultHead()

    body(classes = "text-center") {
        div(classes = "container d-flex w-100 h-100 mx-auto flex-column") {
            header(classes = "masthead") {
                div(classes = "inner") {
                    h3(classes = "masthead-brand") {
                        +"Travel App"
                    }
                    bookNavigation()
                }
            }
            div {
                classes = setOf("container")
                id = "bookingTrip"
                h1(classes = "masthead") {
                    +"Book your trip"
                }
                button {
                    classes = setOf("btn btn-primary")
                    type = ButtonType.button
                    attributes["data-toggle"] = "collapse"
                    attributes["data-target"] = "#collapseExample"
                    attributes["aria-expanded"] = "false"
                    attributes["aria-controls"] = "collapseExample"
                    label { +"Book now" }
                }
            }

            div(classes = "collapse card") {
                id = "collapseExample"
                div(classes = "card card-header") {
                    div(classes = "bc-icons-2") {
                        ol(classes = "breadcrumb cyan lighten-4") {
                            li(classes = "breadcrumb-item active") {
                                a(classes = "black-text") {
                                    href = "#"
                                    +"Destinations"
                                }
                                i(classes = "fa fa-angle-left mx-2") {
                                    attributes["aria-hidden"] = "true"
                                }
                            }
                            li(classes = "breadcrumb-item") {
                                a(classes = "black-text") {
                                    +"Details"
                                }
                                i(classes = "fa fa-angle-left mx-2") {
                                    attributes["aria-hidden"] = "true"
                                }
                            }
                            li(classes = "breadcrumb-item") {
                                +"Confirmation"
                            }
                        }
                    }
                }

                div(classes = "card-body mx-auto") {
                    form("/book", method = FormMethod.post)
                    {
                        classes = setOf("form-group")
                        acceptCharset = "utf-8"

                        label { +"Full Name: " }
                        textInput {
                            classes = setOf("form-control")
                            name = "fullName"
                            placeholder = "Full Name"

                        }
                        label { +"Email: " }
                        textInput {
                            classes = setOf("form-control")
                            name = "email"
                            placeholder = "Your Email"

                        }
                        label { +"Select destination" }
                        select(classes = "form-control") {
                            name = "destination"
                            id = "destinationSelect"
                            option {
                                +"Japan"
                            }
                            option {
                                +"South-Korea"
                            }
                        }
                        button(classes = "btn btn-success") {
                            id = "sendBtn"
                            +"Send"
                        }
                    }
                }

                div(classes = "card-footer") {
                    button {
                        classes = setOf("btn btn-danger")
                        +"Go back"
                    }
                    button {
                        classes = setOf("btn btn-warning")
                        id = "closeBtn"
                        +"Close"
                    }
                }


            }


            defaultFooter()
        }

        script {
            src = "https://code.jquery.com/jquery-3.2.1.slim.min.js"
        }
        script {
            src =
                "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        }
        script {
            src = "/static/bootstrap.js"
        }
        script {
            src = "/static/booking.js"
        }
    }
}