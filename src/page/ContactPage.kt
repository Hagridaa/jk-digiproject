package me.travel.page

import kotlinx.html.*

fun DIV.contactNavigation() {
    nav(classes = "nav nav-masthead justify-content-center") {
        a(href = "/", classes = "nav-link") {
            +"Home"
        }
        a(href = "/book", classes = "nav-link") {
            +"Book"
        }
        a(href = "/destinations", classes = "nav-link") {
            +"Destinations"
        }
        a(href = "/gallery", classes = "nav-link") {
            +"Gallery"
        }
        a(href = "#", classes = "nav-link active") {
            +"Contact"
        }
        a(href = "/login", classes = "nav-link") {
            +"Login"
        }
    }
}

fun HTML.contactPage() {
    lang = "en"
    defaultHead()

    body(classes = "text-center") {
        div(classes = "container d-flex w-100 h-100 mx-auto flex-column") {
            header(classes = "masthead") {
                div(classes = "inner") {
                    h3(classes = "masthead-brand") {
                        +"Travel App"
                    }
                    contactNavigation()
                }
            }

            div(classes = "mx-auto flex-column") {
                h2(classes = "masthead text-center") {
                    +"Any questions?"
                }
                div(classes = "card") {
                    ul(classes = "list-group list-group-flush") {
                        li(classes = "list-group-item") {
                            b { +"Phone: " }
                            +"020 111 1102"
                        }
                        li(classes = "list-group-item") {
                            b { +"Address: " }
                            +"Tiukutie 6, 0001 Tiuku"
                        }

                    }
                }
            }

            div(classes = "text-center") {
                button {
                    classes = setOf("btn btn-info")
                    type = ButtonType.button
                    attributes["data-toggle"] = "collapse"
                    attributes["data-target"] = "#collapseExample2"
                    attributes["aria-expanded"] = "#false"
                    attributes["aria-controls"] = "#collapseExample2"
                    +"Send us mail!"
                }

                div(classes = "collapse") {
                    id = "collapseExample2"
                    div(classes = "container w-50") {
                        id = "mail-form"
                        div(classes = "col-lg-12") {
                            form("/book", method = FormMethod.post) {
                                +"Message: "
                                br { }
                                textArea {
                                    id = "message"
                                    placeholder = "What's on your mind?"
                                }
                                +"Name: "
                                br { }
                                input {
                                    type = InputType.text
                                    id = "name"
                                    name = "name"
                                    placeholder = "Name"
                                }
                                br { }
                                +"Email: "
                                br { }
                                input {
                                    type = InputType.text
                                    id = "email"
                                    name = "name"
                                    placeholder = "Your email"
                                }
                                br { }
                                br { }
                                button(classes = "btn btn-success") {
                                    type = ButtonType.submit
                                    +"Send"
                                }
                            }
                        }
                    }
                }

            }


            defaultFooter()
        }

        script {
            src = "https://code.jquery.com/jquery-3.2.1.slim.min.js"
        }
        script {
            src =
                "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        }
        script {
            src = "/static/bootstrap.js"
        }
        script {
            src = "/static/booking.js"
        }
    }
}