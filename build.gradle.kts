import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val logback_version: String by project
val ktor_version: String by project
val kotlin_version: String by project

plugins {
    application
    kotlin("jvm") version "1.3.30"
}

apply {
    plugin("com.google.cloud.tools.appengine")
}
    buildscript {
        repositories {
            google()
            jcenter()
        }
        dependencies {
            classpath("com.google.gms:google-services:4.2.0")
            classpath("com.google.cloud.tools:appengine-gradle-plugin:1.3.4")
        }
    }
    group = "travel-asia"
    version = "0.0.2"

    allprojects {
        repositories {
            google()
            jcenter()
            maven { url = uri("https://maven.google.com") }
            maven { url = uri("https://kotlin.bintray.com/ktor") }
            maven { url = uri("https://kotlin.bintray.com/kotlin-js-wrappers") }
        }
    }
    application {
        mainClassName = "io.ktor.server.netty.EngineMain"
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven { url = uri("https://maven.google.com") }
        "http://dl.bintray.com/kotlin".let {
            maven { setUrl("$it/ktor") }
            maven { setUrl("$it/kotlinx") }
            maven { setUrl("$it/kotlin-js-wrappers") }
        }
    }
}

application {
    applicationName = "travel-asia"
    mainClassName = "me.travel.KtorServerKt"
    group = "travel-asia"
    version = "0.0.2"
}

dependencies {
    implementation("com.google.firebase:firebase-core:16.0.9")
    implementation("com.google.firebase:firebase-database:17.0.0")
    implementation("com.google.firebase:firebase-database-connection:16.0.2")
    implementation("com.google.firebase:firebase-admin:6.8.1")
    implementation((kotlin("reflect")))
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    compile("io.ktor:ktor-server-netty:$ktor_version")
    compile("ch.qos.logback:logback-classic:$logback_version")
    compile("io.ktor:ktor-server-core:$ktor_version")
    compile("io.ktor:ktor-html-builder:$ktor_version")
    compile("org.jetbrains:kotlin-css-jvm:1.0.0-pre.31-kotlin-1.2.41")
    compile("io.ktor:ktor-freemarker:$ktor_version")
    compile("io.ktor:ktor-gson:$ktor_version")
    compile("io.ktor:ktor-server-host-common:$ktor_version")
    compile("io.ktor:ktor-auth:$ktor_version")
    compile("io.ktor:ktor-auth-jwt:$ktor_version")
    compile("io.ktor:ktor-server-sessions:$ktor_version")
    testCompile("io.ktor:ktor-server-tests:$ktor_version")
}

        compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
        compile("io.ktor:ktor-server-netty:$ktor_version")
        compile("ch.qos.logback:logback-classic:$logback_version")
        compile("io.ktor:ktor-server-core:$ktor_version")
        compile("io.ktor:ktor-html-builder:$ktor_version")
        compile("org.jetbrains:kotlin-css-jvm:1.0.0-pre.31-kotlin-1.2.41")
        /*compile("io.ktor:ktor-server-host-common:$ktor_version")*/
        compile("io.ktor:ktor-auth:$ktor_version")
        compile("io.ktor:ktor-auth-jwt:$ktor_version")
        compile("io.ktor:ktor-server-sessions:$ktor_version")
        testCompile("io.ktor:ktor-server-tests:$ktor_version")
        
    }

    kotlin.sourceSets["main"].kotlin.srcDirs("src")
    kotlin.sourceSets["test"].kotlin.srcDirs("test")

    sourceSets["main"].resources.srcDirs("resources")
    sourceSets["test"].resources.srcDirs("testresources")

    val build: DefaultTask by tasks

    val clean: Delete by tasks
    task("stage") {
        dependsOn(build, clean)
    }
