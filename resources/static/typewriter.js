const instance = new TypeIt('.cover-heading', {
  strings: ['Travel the World', 'Travel the Asia'],
  breakLines: false,
  waitUntilVisible: true
}).go();