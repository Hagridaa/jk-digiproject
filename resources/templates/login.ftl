<#-- @ftlvariable name="data" type="me.travel.IndexData" -->
<html lang="en">
<head>
    <link rel="stylesheet" href="/static/styles.css">
    <title>Travel The Asia - Login</title>
</head>
<body>
<#if error??>
    <p style="color:red;">${error}</p>
</#if>
<form action="/login" method="post" enctype="application/x-www-form-urlencoded">
    <div>User:</div>
    <div><label>
            <input type="text" name="username"/>
        </label></div>
    <div>Password:</div>
    <div><label>
            <input type="password" name="password"/>
        </label></div>
    <div><input type="submit" value="Login"/></div>
</form>
</body>
</html>