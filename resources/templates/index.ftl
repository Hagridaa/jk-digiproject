<#-- @ftlvariable name="data" type="me.travel.IndexData" -->
<html lang="en">
<head>
    <link rel="stylesheet" href="/static/bootstrap.css">
    <link rel="stylesheet" href="/static/styles.css">
    <title>Travel The Asia</title>
</head>
    <body>
        <ul>
        <#list data.items as item>
            <li>${item}</li>
        </#list>
        </ul>
    </body>
</html>
